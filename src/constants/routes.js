export const LANDING = '/';

// this route can change
export const SIGN_UP = '/signup';
// this route can change
export const SIGN_IN = '/signin';
// this route can change
export const PASSWORD_FORGET = '/pw-forget';

export const ACCOUNT = '/account';
export const CONTACT_PAGE = '/kontakt';
export const CONTACT_FORM = '/kontakt/formularz';
export const EVENTS_PAGE = '/wydarzenia';
export const EVENTS_FORM = '/wydarzenia/formularz';
export const ORGANIZATIONS_PAGE = '/organizacje';
export const ORGANIZATIONS_FORM = '/organizacje/formularz';
export const CONTESTS_LOTTERIES_PAGE = '/konkursy-loterie';
export const DONORS_PAGE = '/darczyncy';