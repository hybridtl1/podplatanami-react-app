import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';


// Initialize connection configuration
const prodConfig = {
    apiKey: "AIzaSyCgUmbs0WSyI_h8SLxa3Q2bDbdAx7sH-8E",
    authDomain: "podplatanami-2ae7d.firebaseapp.com",
    databaseURL: "https://podplatanami-2ae7d.firebaseio.com",
    projectId: "podplatanami-2ae7d",
    storageBucket: "podplatanami-2ae7d.appspot.com",
    messagingSenderId: "43072992968"
  };
  const devConfig = {
    apiKey: "AIzaSyCgUmbs0WSyI_h8SLxa3Q2bDbdAx7sH-8E",
    authDomain: "podplatanami-2ae7d.firebaseapp.com",
    databaseURL: "https://podplatanami-2ae7d.firebaseio.com",
    projectId: "podplatanami-2ae7d",
    storageBucket: "podplatanami-2ae7d.appspot.com",
    messagingSenderId: "43072992968"
  };
  const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

  // Initialize Firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }

  // Do Auth
  const auth = firebase.auth();
  
  // Get DB ref
  const db = firebase.database();

  export {
      auth,
      db,
  };