import React, { Component } from 'react';
import _ from 'lodash';
import './Ngos.css';
import SingleNgo from './SingleNgo';
import InfiniteScroll from 'react-infinite-scroller';


class Ngos extends Component {
    constructor() {
        super();
        this.state = {
            items: [],
            nextItem:0,
            hasMore:true,
            page_size:10,
            previousPageSize:0
        }; 
    };


    

    componentDidMount() {
    }

    loadItems(){
        var self = this;
        var items = self.state.items;
        fetch('https://20180703t192652-dot-organizations-service-dot-htltest-207215.appspot.com/organizationsPagination/'+self.state.nextItem+'/'+self.state.page_size)
        .then(response => response.json())
        .then(resources => {
            _.values(resources.organizations).map(resource=>{
                items.push(resource);
            })
            
            var _hasMore = (items.length === self.state.previousPageSize + self.state.page_size);
            
            this.setState({
                items: items,
                nextItem:items[items.length-1].creationDate+1,
                hasMore: _hasMore,
                page_size: self.state.page_size,
                previousPageSize: items.length
            }, () => {
            });
        });

        return 0;
    }

    render() {
        return(
            <div className="ngosComponent">

                <div className="backgroundGray">
                    <div className="ngosContainer">
                        <InfiniteScroll
                            className="infiniteScroll"
                            pageStart={0}
                            loadMore={this.loadItems.bind(this)}
                            hasMore={this.state.hasMore}
                            //loader={<div className="loader" key={0}>Loading ...</div>}
                        >
                            {this.state.items.map((ngo) => {
                                return (
                                    <SingleNgo ngo = {ngo} key = {ngo.name}/>
                                )
                                
                            })} 
                        </InfiniteScroll>
                    </div>
                </div>
            </div>
        )

    }
}

export default Ngos;