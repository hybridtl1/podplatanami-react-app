import React, { Component } from 'react';
import _ from 'lodash';
import './Events.css';
import SingleEvent from './SingleEvent';
import SingleCategory from './SingleCategory/SingleCategory';
import InfiniteScroll from 'react-infinite-scroller';


class Events extends Component {
    constructor(props){
        super(props);
        console.log(props);
        this.state = {
            categories: [
                {
                    'id':1,
                    'name':'Local Guiding'
                },
                {
                    'id':2,
                    'name':'Teaching'
                },
                {
                    'id':3,
                    'name':'Delivery'
                },
                {
                    'id':4,
                    'name':'Selling Online'
                },
                {
                    'id':5,
                    'name':'Driving'
                },
                {
                    'id':6,
                    'name':'Private Car/Vehicle Rental'
                },
                {
                    'id':7,
                    'name':'Pet Sitting/Walking'
                },
                {
                    'id':8,
                    'name':'Cleaning'
                },
                {
                    'id':9,
                    'name':'Freelance'
                },
                {
                    'id':10,
                    'name':'Babysitting'
                },
                {
                    'id':11,
                    'name':'Other Jobs'
                },
                {
                    'id':12,
                    'name':'Private Home/Space Rental'
                },
                {
                    'id':13,
                    'name':'Other Rental'
                },
                {
                    'id':14,
                    'name':'Home-Cooking'
                }
            ],
            activeCategories:[],
            items: props.eventsInit.events,
            nextItem:props.eventsInit.nextItem,
            hasMore:props.eventsInit.hasMore,
            page_size:props.eventsInit.page_size,
            previousPageSize:props.eventsInit.previousPageSize
        }; 
        console.log('aaa');
        console.log(this.state);
    };


    componentWillMount (){
    }

    componentDidMount() {
        this.toggleCategory = this.toggleCategory.bind(this)
        this.clearCategoryFilter = this.clearCategoryFilter.bind(this)
    }

    clearCategoryFilter = (e) => {
        this,this.setState({activeCategories:[]});
    }

    toggleCategory = (_id) => {
        var _activeCategories = this.state.activeCategories;
        var _active = false;
        _activeCategories.map((category,index)=> {
            if(category === _id) {
                _active = true;
                delete _activeCategories[index];
            }
        });
        if(_active === false)
            _activeCategories.push(_id);
        var temp = [];
        for(let i of _activeCategories)
            i && temp.push(i);
        _activeCategories = temp;
        this.setState({activeCategories: _activeCategories}, function(){
        });
    }

    loadItems(){
        var self = this;
        var items = self.state.items;
        fetch('https://events-service-dot-htltest-207215.appspot.com/eventsPagination/'+self.state.nextItem+'/'+self.state.page_size)
        .then(response => response.json())
        .then(resources => {
            _.values(resources.events).map(resource=>{
                items.push(resource);
            })
            
            var _hasMore = (items.length === self.state.previousPageSize + self.state.page_size);
            
            this.setState({
                items: items,
                nextItem:items[items.length-1].creationDate+1,
                hasMore: _hasMore,
                page_size: self.state.page_size,
                previousPageSize: items.length
            }, () => {
            });
        });
        return 0;
    }

    render() {
        return(
            <div className="eventsComponent">
                <div className="backgroundWhite">
                    <div className="categoriesContainer">
                        <div className="categories">
                            <i className="fa fa-filter"></i> 
                            <span className="ui header">Pick your event categories</span> 
                            <div className="categoriesList">
                                <ul>
                                    <li className={ this.state.activeCategories.length === 0 ? "activeCategoryBtn" : ""} onClick={this.clearCategoryFilter}>
                                        <a>
                                            <span>
                                                All Categories
                                            </span>
                                        </a>
                                    </li>
                                    {this.state.categories.map((singleCategory) => {        
                                        var _active = false;

                                        this.state.activeCategories.map((category)=>{
                                            if(category === singleCategory.id){
                                                _active = true;
                                            }
                                        });

                                        return (
                                            <SingleCategory singleCategory = {singleCategory} active = {_active} toggleCategory={this.toggleCategory} key = {singleCategory.name}/>
                                        )
                                    })} 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="backgroundGray">
                    <div className="eventsContainer">
                        <div className="categoriesContainerMobile">
                            <a>
                                <i className="fa fa-filter"></i> 
                                <span className="ui header">Pick your event categories</span> 
                            </a>
                        </div>
                        <InfiniteScroll
                            className="infiniteScroll"
                            pageStart={0}
                            loadMore={this.loadItems.bind(this)}
                            hasMore={this.state.hasMore}
                        >
                            {this.state.items.map((event) => {
                                var _activeCategory = false;
                                if(event.category)
                                    event.category.map(eventCategory => {
                                        this.state.activeCategories.map((category)=>{
                                            if(category === eventCategory){
                                            _activeCategory = true;
                                            }
                                        });
                                    });
                                if(_activeCategory || this.state.activeCategories.length === 0){
                                    return (
                                        <SingleEvent event = {event} key = {event.creationDate}/>
                                    )
                                }
                            })} 
                        </InfiniteScroll>
                    </div>
                </div>
            </div>
        )
    }
}

export default Events;