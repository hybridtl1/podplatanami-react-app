import React, { Component } from 'react';

class SingleCategory extends Component {
    
        constructor(props) {
            super(props);
            this.state = {
                //active: false
            };             
            
        };

        componentDidMount(){
            this.toggleCategory = this.toggleCategory.bind(this)
        }

        toggleCategory = (e) => {
            //var _active = this.props.singleCategory.active ? true : false;
            this.props.toggleCategory(this.props.singleCategory.id);
            //this.props.singleCategory.active = !_active;


            this.setState({}, function () {
                
            });
        }

        render() { 
            return (
                <li className={ this.props.active ? "activeCategoryBtn" : ""} onClick={this.toggleCategory}>
                    <a>

                        <span>
                            {this.props.singleCategory.active}
                        </span>
                        <span>
                            {this.props.singleCategory.name}
                        </span>
                    </a>
                </li>
            )
        }
    }
    
    export default SingleCategory;