
import React, { Component } from 'react';
import { Link,
         withRouter, 
} from 'react-router-dom';

import { auth, usersDB } from '../firebase';
import * as routes from '../constants/routes';

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
});

// SignUpPage Component
const SignUpPage = ({ history }) =>
  <div>
    <h1>REJESTRIREN</h1>
    <SignUpForm history={history} />
  </div>

// SignUpForm Component
class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = { INITIAL_STATE }
  }

  onSubmit = (event) => {
    const {
      username,
      email,
      passwordOne,
    } = this.state;

    const {
        history,
    } = this.props;


    auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        //create user entity binded row
          usersDB.doCreateUser(authUser.user.uid, username, email).then(
            () => {
              this.setState(() => ({ INITIAL_STATE }));
              history.push(routes.ACCOUNT);
              })
              .catch(error => {
                this.setState(byPropKey('error', error));
                console.log(error);
              });
          //authUser.email = email;
          //history.push(routes.LANDING);
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
        console.log(error);
      });
    event.preventDefault();
    }


  render() {
    const {
        username,
        email,
        passwordOne,
        passwordTwo,
        error,
      } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '' ||
      username === '';


    return (
        <form onSubmit={this.onSubmit}>
            <input
            value={username}
            onChange={event => this.setState(byPropKey('username', event.target.value))}
            type="text"
            placeholder="Imię i Nazwisko"
            />
            <input
            value={email}
            onChange={event => this.setState(byPropKey('email', event.target.value))}
            type="text"
            placeholder="Twój adres e-mail"
            />
            <input
            value={passwordOne}
            onChange={event => this.setState(byPropKey('passwordOne', event.target.value))}
            type="password"
            placeholder="Hasełko kurwa"
            />
            <input
            value={passwordTwo}
            onChange={event => this.setState(byPropKey('passwordTwo', event.target.value))}
            type="password"
            placeholder="i potwierdź do chuja wafla!"
            />
            <button disabled={isInvalid} type="submit">
            Dawaij łajzo!
            </button>

            { error && <p>{error.message}</p> }
        </form>
    );
  }
}

// SignUpLink Component
const SignUpLink = () =>
  <p>
    Dawai harasho nePizdij me tu! Gawarić!
    {' '}
    <Link to={routes.SIGN_UP}>Dawaij registrchujen!</Link>
  </p>

export default withRouter(SignUpPage);

export {
  SignUpForm,
  SignUpLink,
};