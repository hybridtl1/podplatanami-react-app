import React, { Component } from 'react';
import { BrowserRouter as Router,
         Route, 
        } from 'react-router-dom';

import logo from './Logo-podPlatanami.png';
import './App.css'; 

import * as routes from '../constants/routes';
import Navigation from './Navigation';
import LandingPage from './Landing';
import AccountPage from './Account';
import SignInPage from './SignIn';
import SignUpPage from './SignUp';
import PasswordForgetPage from './PasswordForget';
//import EventsPage from './Events';
import Events from './events/Events'
import OrganizationsPage from './Organizations';

import { firebase } from '../firebase';
import withAuthentication from './withAuthentication';


/*
class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      authUser: null,
    };
  }

  componentDidMount() {
    firebase.auth.onAuthStateChanged(authUser => {
      authUser
        ? this.setState(() => ({ authUser }))
        : this.setState(() => ({ authUser: null }));
    });
  }
  */

class App extends React.Component {
  constructor(props){
    super(props);
    console.log(props);
  }

  render(){
    //console.log(this.props);
    return(
        <Router>
          <div>
            <Navigation />

            <hr/>

            <Route
              exact path={routes.LANDING}
              component={() => <LandingPage />}
            />
            <Route
              exact path={routes.SIGN_UP}
              component={() => <SignUpPage />}
            />
            <Route
              exact path={routes.SIGN_IN}
              component={() => <SignInPage />}
            />
            <Route
              exact path={routes.PASSWORD_FORGET}
              component={() => <PasswordForgetPage />}
            />
            <Route
              exact path={routes.ACCOUNT}
              component={() => <AccountPage />}
            />
            <Route
              exact path={routes.EVENTS_PAGE}
              component={() => <Events eventsInit={this.props.events}/>}
            />      
            <Route
            exact path={routes.ORGANIZATIONS_PAGE}
            component={() => <OrganizationsPage />}
          />
          </div>
      </Router>);
    }
  }


export default App;//withAuthentication(App);
