import React from 'react';
import { Link } from 'react-router-dom';

import * as routes from '../constants/routes';
import SignOutButton from './SignOut';
import AuthUserContext from './AuthUserContext';

const Navigation = () =>
  <AuthUserContext.Consumer>
    { authUser => authUser ? <NavigationAuth/> : <NavigationNonAuth/> }
  </AuthUserContext.Consumer>

const NavigationAuth = () =>
    <ul>
        <li><Link to={routes.EVENTS_PAGE}>WYDARZENIA</Link></li>
        <li><Link to={routes.ORGANIZATIONS_PAGE}>ORGANIZACJE</Link></li>
        <li><Link to={routes.CONTESTS_LOTTERIES_PAGE}>KONKURSY</Link></li>
        <li><Link to=''>AUKCJE DOBROCZYNNE</Link></li>
        <li><Link to={routes.DONORS_PAGE}>DARCZYŃCY</Link></li>
        <li><Link to={routes.ACCOUNT}>Panel Użytkownika</Link></li>
        <li><Link to="/SignUp" >DODAJ</Link></li>
        <li><SignOutButton /></li>
    </ul>

const NavigationNonAuth = () =>
    <ul>
        <li><Link to={routes.EVENTS_PAGE}>WYDARZENIA</Link></li>
        <li><Link to={routes.ORGANIZATIONS_PAGE}>ORGANIZACJE</Link></li>
        <li><Link to={routes.CONTESTS_LOTTERIES_PAGE}>KONKURSY</Link></li>
        <li><Link to=''>AUKCJE DOBROCZYNNE</Link></li>
        <li><Link to={routes.DONORS_PAGE}>DARCZYŃCY</Link></li>
        <li><Link to={routes.SIGN_IN}>ZALOGUJ</Link></li>
        <li><Link to="/SignUp" >DODAJ</Link></li>
    </ul>

export default Navigation;