import React, { Component } from 'react';

import AuthUserContext from './AuthUserContext';
import { PasswordForgetForm } from './PasswordForget';
import PasswordChangeForm from './PasswordChange';
import withAuthorization from './withAuthorization';
import { usersDB } from '../firebase';


class AccountPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: null,
    };
  }

  componentDidMount() {
    usersDB.onceGetUsers().then(snapshot => {
      this.setState(() => ({ users: snapshot.val() }))
    });
  }

  render() {
    const { users } = this.state;

    return(
      <AuthUserContext.Consumer>
        { authUser =>
          <div>
            <h1>Panel tego użytkoheńka: {authUser.email} </h1>
            
            { !!users && <UserList users={users}/> }

            <div><br></br>---------<br></br></div>
            
            <PasswordForgetForm />
            <PasswordChangeForm />
          </div>
        }
      </AuthUserContext.Consumer>
    )
  }
}

const UserList = ({ users }) =>
  <div>
    <h2>Lista użytkoheńków</h2>
    <p> (harasho!) </p>

    {Object.keys(users).map(key =>
      <div key={key}>{users[key].username}</div>
    )}
  </div>

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(AccountPage);