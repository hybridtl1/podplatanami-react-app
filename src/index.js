import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import _ from 'lodash';

//events initial data
var eventsInit={};
eventsInit.events = [];
eventsInit.hasMore = true;
eventsInit.page_size = 10;
eventsInit.previousPageSize = 0;
eventsInit.nextItem = 0;

//organizations initial data
fetch('https://events-service-dot-htltest-207215.appspot.com/eventsPagination/'+eventsInit.nextItem+'/'+eventsInit.page_size)
.then(response => response.json())
.then(resources => {
    _.values(resources.events).map(resource=>{
        eventsInit.events.push(resource);
    })
    eventsInit.nextItem = eventsInit.events[eventsInit.events.length-1].creationDate+1;
    
    eventsInit.hasMore = (eventsInit.events.length === eventsInit.previousPageSize + eventsInit.page_size);
    eventsInit.previousPageSize = eventsInit.events.length





    ReactDOM.render(<App events={eventsInit}/>, document.getElementById('root'));
});

registerServiceWorker();